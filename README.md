# Software Stack
- [Plex](http://plex.tv/)
- [Jellyfin](https://jellyfin.org/)
- [Sonarr](https://sonarr.tv/)
- [Radarr](https://radarr.video/)
- [Jackett](https://github.com/Jackett/Jackett)
- [Transmission](https://transmissionbt.com/) & [OpenVPN](https://openvpn.net/)
- [Traefik](http://traefik.io)



## Compatible architecture
- ARM v7 (Rasperry Pi B)
- ARM v8 (Rasperry Pi B+)

## Prerequisites
- docker
- docker-compose

## Installing Docker & docker-compose on Raspberry Pi

* docker
```bash
curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh
sudo groupadd docker
sudo gpasswd -a $USER docker
newgrp docker
```

* docker-compose
```bash
sudo pip install docker-compose
```

please refer to the official documentation ([here](https://docs.docker.com/install/) and [here](https://docs.docker.com/compose/install/#install-compose)) for more informations.

# How to install

* Clone this repositery on your Raspberry Pi
```bash
git clone git@gitlab.com:Darathor/media-center-docker-raspberry-pi.git
```
* Edit the file sample.env to your need and then rename it .env
```bash
cd media-center-docker-raspberry-pi
vi sample.env
mv sample.env .env
```

Launch the stack
```bash
docker-compose up -d
```

# Configurations of each software

All configuration files will be in `media-center-docker-raspberry-pi/services/<service>`
